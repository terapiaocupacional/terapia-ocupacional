<?php
/**
 * Created by PhpStorm.
 * User: ivanortega
 * Date: 20/03/2019
 * Time: 15:48
 */
//session_start();

$relativePath = "./";
include_once ('./includes/includes.php');
$titol = t("Usuari");


if(!isset($_SESSION['admin'])) {
    header("Location: backoffice/index.php");
}else if($_SESSION['admin'] == '1'){
    header("Location: backoffice/index.php");
}else{
    //header("Location: frontend_categories.php");
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    include_once('./includes/pacient/header.php');
    ?>
</head>
<body>

    <header>
        <h1 style="text-transform: capitalize;"><?=$_SESSION['nom'];?></h1>
        <data value="02/05/2019"><?=strftime("%A, %d de %B de %Y", $miFecha)?></data>
    </header>
    <!--/////////////////// Menu ////////////////////////////-->
    <?php include_once('./includes/pacient/menu.php'); ?>
    <!--/////////////////// Menu ////////////////////////////-->

    <div class="contenidor">
        <a href="backoffice/content/logout.php"> <button type="button" class="btn btn-danger">Log Out</button></a>
    </div>

    <?php include_once ('./includes/pacient/scripts.php'); ?>
</body>
</html>
