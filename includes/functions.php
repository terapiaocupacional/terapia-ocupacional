<?php
/**
 * Created by PhpStorm.
 * User: ivanortega
 * Date: 11/03/2019
 * Time: 18:17
 */

include_once("connection.php");



// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

function maxIdDesc(){
    $conn = $GLOBALS['conn'];
    $sql = "SELECT max(Id) as midDes FROM traduccions";


    $result = mysqli_query($conn, $sql);
    while($row = $result->fetch_array())
    {
        $max = $row["midDes"];
    }


    if(isset($max)){
        //echo implode($rows);
        return intval($max) + 1;
    }else{
        return 0;
    }

}

function t($value){
    $conn = $GLOBALS['conn'];
    $value = str_replace("'","''",$value);
    $sql = "SELECT * FROM traduccions WHERE Traduccio like '".$value."'";

    $result = mysqli_query($conn, $sql);

    if($row = $result->fetch_array())
    {
        $desc = $row["Traduccio"];

        $sql = "SELECT * FROM traduccions WHERE Id = ".$row["Id"]." AND Id_Idioma = ".$GLOBALS['idioma'];
        $result = mysqli_query($conn, $sql);
        if($row = $result->fetch_array()){
            $desc = $row["Traduccio"];
        }
    }

    if(isset($desc)){
        return $desc;
    }else {
        $value = mysqli_real_escape_string($conn, $value);
        $sql = "INSERT INTO traduccions (Id_Idioma,Id,Traduccio) VALUES (1,'" . maxIdDesc() . "','" . $value . "')";
        $conn->query($sql);
        return $value;
    }


}


function uploadFile(){
    $conn = $GLOBALS['conn'];

    if (isset($_POST['save'])) { // if save button on the form is clicked
        // name of the uploaded file
        $filename = $_FILES['myfile']['name'];

        // destination of the file on the server
        $destination = 'uploads/' . $filename;

        // get the file extension
        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        // the physical file on a temporary uploads directory on the server
        $file = $_FILES['myfile']['tmp_name'];
        $size = $_FILES['myfile']['size'];

        if (!in_array($extension, ['zip', 'pdf', 'docx'])) {
            echo "You file extension must be .zip, .pdf or .docx";
        } elseif ($_FILES['myfile']['size'] > 1000000) { // file shouldn't be larger than 1Megabyte
            echo "File too large!";
        } else {
            // move the uploaded (temporary) file to the specified destination
            if (move_uploaded_file($file, $destination)) {
                $sql = "INSERT INTO files (name, size, downloads) VALUES ('$filename', $size, 0)";
                if (mysqli_query($conn, $sql)) {
                    echo "File uploaded successfully";
                }
            } else {
                echo "Failed to upload file.";
            }
        }
    }
}

function getRutines($usuari){
    $fet = false;
    $avui = true;
    $style = "";
    $cont = 0;
    $contTotal = 0;
    $first = true;
    $conn = $GLOBALS['conn'];
    $sql = "SELECT r.*, w.Data, w.Fet, w.Id as IdReal FROM wo_usr w left join rutina r on w.Id_Rutina = r.Id WHERE w.Id_Username = '".$usuari."' and w.Fet = 0 Order by w.Data ASC";
    $result = mysqli_query($conn, $sql);
    echo ('<div class="list-group">');
    while($row = $result->fetch_array()){
        $style = "";
        if($first){
            $style = " active";
            $first = false;
        }else{
            $style = " disabled";
        }

        if($row['Fet'] > 0){

            $style = " hidden";
        }else{

        }
        if($row['Data'] == date("Y-m-d")){
            //$style = " active";
        }else if($row['Data'] >  date("Y-m-d") ){
            $style = " hidden";
        }
        if($style == " hidden"){
            $cont += 1;
        }
        echo('    <a href="'.$_SERVER['PHP_SELF'] .'?Id='.$row['IdReal'].'" class="list-group-item list-group-item-action flex-column align-items-start '.$style.'">');
        echo('        <div class="d-flex w-100 justify-content-between">');
        echo('            <h5 class="mb-1">'.t($row['Nom']) .'</h5>');
        echo('            <small>'.$row['Data'].'</small>');
        echo('        </div>');
        echo('        <p class="mb-1">'.t($row['Descripcio']).'</p>');
        echo('    </a>');

        $contTotal += 1;
    }
    echo('</div>');
    if($contTotal == $cont){
        $sql = "SELECT r.*, w.Data, w.Fet, w.Id as IdReal FROM wo_usr w left join rutina r on w.Id_Rutina = r.Id WHERE w.Id_Username = '".$usuari."' Order by w.Data ASC";
        $result = mysqli_query($conn, $sql);
        echo ('<div class="list-group">');
        echo('<H1 class="text-primary">'.t("Propera-ment").'</H1>');
        while($row = $result->fetch_array()){

            if($row['Data'] <=  date("Y-m-d") ){
                $style = " hidden";
            }else{
                $style = " disabled";
            }

            echo('    <a href="'.$_SERVER['PHP_SELF'] .'?Id='.$row['IdReal'].'" class="list-group-item list-group-item-action flex-column align-items-start list-group-item-dark '.$style.'">');
            echo('        <div class="d-flex w-100 justify-content-between">');
            echo('            <h5 class="mb-1">'.t($row['Nom']) .'</h5>');
            echo('            <small>'.$row['Data'].'</small>');
            echo('        </div>');
            echo('        <p class="mb-1">'.t($row['Descripcio']).'</p>');
            echo('    </a>');

            $contTotal += 1;
        }
        echo('</div>');
    }

    //return $strrutinas;
}

function getExercicis($idRutines){
    $exercicis = array();
    $conn = $GLOBALS['conn'];
    $sql = "SELECT * FROM exercici inner join rutineslin on exercici.Id = rutineslin.Id_exercici where rutineslin.Id_Rutina in(".$idRutines.")";
    $result = mysqli_query($conn, $sql);
    $contador = 0;
    $strExercicis = "";
    while($row = $result->fetch_array()){
        $exercicis[$contador] = $row['exercicis.Id'];
        $contador += 1;
    }
    foreach ($exercicis as $exercici){
        $strExercicis .= $exercici.",";
    }
    $strExercicis = substr($strExercicis,0,-1);
    return $strExercicis;
}
?>



