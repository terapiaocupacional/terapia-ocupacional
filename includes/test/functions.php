<?php
/**
 * Created by PhpStorm.
 * User: ivanortega
 * Date: 11/03/2019
 * Time: 18:17
 */

include_once("connection.php");



// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

function maxIdDesc(){
    $conn = $GLOBALS['conn'];
    $sql = "SELECT max(Id) as midDes FROM traduccions";


    $result = mysqli_query($conn, $sql);
    while($row = $result->fetch_array())
    {
        $max = $row["midDes"];
    }


    if(isset($max)){
        //echo implode($rows);
        return intval($max) + 1;
    }else{
        return 0;
    }

}

function t($value){
    $conn = $GLOBALS['conn'];
    $value = str_replace("'","''",$value);
    $sql = "SELECT * FROM traduccions WHERE Traduccio like '".$value."'";

    $result = mysqli_query($conn, $sql);

    if($row = $result->fetch_array())
    {
        $desc = $row["Traduccio"];

        $sql = "SELECT * FROM traduccions WHERE Id = ".$row["Id"]." AND Id_Idioma = ".$GLOBALS['idioma'];
        $result = mysqli_query($conn, $sql);
        if($row = $result->fetch_array()){
            $desc = $row["Traduccio"];
        }
    }

    if(isset($desc)){
        return $desc;
    }else {
        $value = mysqli_real_escape_string($conn, $value);
        $sql = "INSERT INTO traduccions (Id_Idioma,Id,Traduccio) VALUES (1,'" . maxIdDesc() . "','" . $value . "')";
        $conn->query($sql);
        return $value;
    }


}


function uploadFile(){
    $conn = $GLOBALS['conn'];

    if (isset($_POST['save'])) { // if save button on the form is clicked
        // name of the uploaded file
        $filename = $_FILES['myfile']['name'];

        // destination of the file on the server
        $destination = 'uploads/' . $filename;

        // get the file extension
        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        // the physical file on a temporary uploads directory on the server
        $file = $_FILES['myfile']['tmp_name'];
        $size = $_FILES['myfile']['size'];

        if (!in_array($extension, ['zip', 'pdf', 'docx'])) {
            echo "You file extension must be .zip, .pdf or .docx";
        } elseif ($_FILES['myfile']['size'] > 1000000) { // file shouldn't be larger than 1Megabyte
            echo "File too large!";
        } else {
            // move the uploaded (temporary) file to the specified destination
            if (move_uploaded_file($file, $destination)) {
                $sql = "INSERT INTO files (name, size, downloads) VALUES ('$filename', $size, 0)";
                if (mysqli_query($conn, $sql)) {
                    echo "File uploaded successfully";
                }
            } else {
                echo "Failed to upload file.";
            }
        }
    }
}

function getRutines($usuari){
    $data = time();
    $contador = 0;
    $rutines = array();
    $conn = $GLOBALS['conn'];
    $sql = "SELECT * from wo_usr where Id_Username = '".$usuari."' and Data='".strftime("%Y/%m/%d",$data)."'";
    $result = mysqli_query($conn, $sql);
    while($row = $result->fetch_array()){
        $rutines[$contador] = $row['Id_Rutina'];
        $contador +=1;
    }
    echo $rutines;
}
?>



