<?php
/**
 * Created by PhpStorm.
 * User: ivanortega
 * Date: 19/03/2019
 * Time: 15:37
 */
include_once("functions.php");




if(!isset($_SESSION['username']) || $_SESSION['username'] == "" ){
    echo $_SESSION['username'];
    if(file_exists('./../login.php')){
        header("Location: ./../login.php");
    }else if(file_exists('./login.php')){
        header("Location: ./login.php");
    }else if(file_exists('../../login.php')){
        header("Location: ../../login.php");
    }else if(file_exists('../../../login.php')){
        header("Location: ../../../login.php");
    }else{
        header("Location: /404.html");
    }

}else{
    if( $_SESSION['admin'] == '0' && isset($_SERVER['REQUEST_URI'])) {

        if (strpos($_SERVER['REQUEST_URI'], "backoffice") == false) {
            //echo "backoffice" . ' not exists in the URL <br>';
        }
        else {
            if(file_exists('./../index.php')){
                header("Location: ".str_replace("/backoffice","",$_SERVER['REQUEST_URI']));
            }
            //echo "backoffice" . '  exists in the URL <br>';
        }
    }
}

function llistaRutinesLinies(){
    $conn = $GLOBALS['conn'];
    $sql = "SELECT l.*, e.Nom as Ex FROM rutineslin l LEFT JOIN exercici e on l.Id_Exercici = e.Id WHERE l.Id_Rutina = ".$_GET['id']." Order by l.Linia asc";


    $result = mysqli_query($conn, $sql);
    while($row = $result->fetch_array())
    {
        echo'<tr>';
        echo'<td>'.$row['Linia'].'</td>';
        echo'<td>'.$row['Ex'].'</td>';
        echo'<td>'.$row['Repeticions'].'</td>';
        echo'<td>'.$row['Temps'].'</td>';
        echo'<td><a href="'.$_SERVER['PHP_SELF'].'?delete='.$row['Id_Rutina'].'&id='.$_GET['id'].'&lin='.$row["Linia"].'"><i class="fas fa-trash"></i></a></td>';
        echo'</tr>';
    }
    //updateLinies();
}

function llistaCategories(){
    $conn = $GLOBALS['conn'];
    $sql = "SELECT * FROM categoria";


    $result = mysqli_query($conn, $sql);
    while($row = $result->fetch_array())
    {
        echo'<tr>';
        echo'<td>'.$row['Nom'].'</td>';
        echo'<td>'.$row['Descripcio'].'</td>';
        $bg = "background-image: url('".$row['Imatge']."'); background-position: center;background-size: cover; height:50px;";
        echo'<td style=" '.$bg.'"></td>';
        echo'<td><a href="'.$_SERVER['PHP_SELF'].'?delete='.$row['Id'].'"><i class="fas fa-trash"></i></a></td>';
        echo'</tr>';
    }
}
function llistaRutines(){
    $conn = $GLOBALS['conn'];


    $sql = "SELECT * FROM rutina Order by Nom asc";


    $result = mysqli_query($conn, $sql);
    while($row = $result->fetch_array())
    {
        echo'<tr>';
        echo'<td>'.$row['Nom'].'</td>';
        echo'<td>'.$row['Descripcio'].'</td>';
        $sql = "SELECT l.*, e.Nom as Ex FROM rutineslin l LEFT JOIN exercici e on l.Id_Exercici = e.Id WHERE l.Id_Rutina = ".$row['Id']." Order by e.Nom asc";
        $Ex = '';
        $results = mysqli_query($conn, $sql);
        while($ro = $results->fetch_array())
        {
            $Ex .= $ro['Ex'].' / ';

        }
        if($Ex !== '')$Ex = substr($Ex, 0, -3);

        echo'<td>'.$Ex.'</td>';
        echo'<td> <a href="'.$_SERVER['PHP_SELF'].'?edit='.$row['Id'].'"><i class="fas fa-pen"></i></a> - ';
        echo '<a href="'.$_SERVER['PHP_SELF'].'?delete='.$row['Id'].'"><i class="fas fa-trash"></i></a></td>';
        echo'</tr>';
    }


}
function OptionRutines(){
    $conn = $GLOBALS['conn'];
    $sql = "SELECT * FROM rutina Order by Nom asc";


    $result = mysqli_query($conn, $sql);
    while($row = $result->fetch_array())
    {
        echo'<option value="'.$row['Id'].'">'.$row['Nom'].'</option>';
    }
}
function OptionCategories(){
    $conn = $GLOBALS['conn'];
    $sql = "SELECT * FROM categoria Order by Nom asc";


    $result = mysqli_query($conn, $sql);
    while($row = $result->fetch_array())
    {
        echo'<option value="'.$row['Id'].'">'.$row['Nom'].'</option>';
    }
}
function llistaExercicis(){
    $conn = $GLOBALS['conn'];
    $sql = "SELECT * FROM exercici Order by Nom asc";


    $result = mysqli_query($conn, $sql);
    while($row = $result->fetch_array())
    {
        echo'<tr>';
        echo'<td>'.$row['Nom'].'</td>';
        echo'<td>'.$row['DescripcioCurta'].'</td>';
        echo'<td><a href="'.$_SERVER['PHP_SELF'].'?delete='.$row['Id'].'"><i class="fas fa-trash"></i></a></td>';
        echo'</tr>';
    }
}

function OptionExercicis(){
    $conn = $GLOBALS['conn'];
    $sql = "SELECT * FROM exercici Order by Nom asc";


    $result = mysqli_query($conn, $sql);
    while($row = $result->fetch_array())
    {
        echo'<option value="'.$row['Id'].'">'.$row['Nom'].'</option>';
    }

}

function OptionUsuaris(){
    $conn = $GLOBALS['conn'];
    $sql = "SELECT * FROM usuaris Order by Nom asc";


    $result = mysqli_query($conn, $sql);
    while($row = $result->fetch_array())
    {
        echo'<option value="'.$row['Username'].'">'.$row['Nom'].'</option>';
    }

}

function llistaUsuaris(){
    $conn = $GLOBALS['conn'];
    $sql = "SELECT u.*, i.Nom as lng FROM usuaris u left join idioma i on i.Id = u.Id_Idioma";

    $result = mysqli_query($conn, $sql);
    while($row = $result->fetch_array())
    {
        echo'<tr>';
        echo'<td>'.$row['Username'].'</td>';
        echo'<td>'.$row['Nom'].'</td>';
        echo'<td>'.$row['lng'].'</td>';
        echo'<td>'.$row['Admin'].'</td>';

        echo'<td><a href="'.$_SERVER['PHP_SELF'].'?delete='.$row['Username'].'"><i class="fas fa-trash"></i></a></td>';
        echo'</tr>';
    }
}
function llistaRutinesAPacients(){
    $conn = $GLOBALS['conn'];
    $sql = "SELECT w.Id, w.Data, r.Nom as Rut, e.Nom as Ex FROM wo_usr w left join usuaris e on e.Username = w.Id_Username left join  rutina r on w.Id_Rutina = r.Id";

    $result = mysqli_query($conn, $sql);
    while($row = $result->fetch_array())
    {
        echo'<tr>';
        echo'<td>'.$row['Rut'].'</td>';
        echo'<td>'.$row['Ex'].'</td>';
        echo'<td>'.$row['Data'].'</td>';

        echo'<td><a href="'.$_SERVER['PHP_SELF'].'?delete='.$row['Id'].'"><i class="fas fa-trash"></i></a></td>';
        echo'</tr>';
    }
}

function OptionIdiomes(){
    $conn = $GLOBALS['conn'];
    $sql = "SELECT * FROM idioma";


    $result = mysqli_query($conn, $sql);
    while($row = $result->fetch_array())
    {
        echo'<option value="'.$row['Id'].'">'.$row['Nom'].'</option>';
    }
}