<?php
/**
 * Created by PhpStorm.
 * User: ivanortega
 * Date: 02/05/2019
 * Time: 20:02
 */

$path = "./";
if(isset($relativePath)){
    $path = $relativePath;
}

?>

<div class="fixed-top sticky-top menuPacient" id="menuPacient">
    <div class="row">

        <div class="col-4 align-middle columna">
            <a class="btn btn-<?php if($titol == t('Exercicis')){ echo'primary';}else{echo'secondary';} ?> btn-sm" href="<?=$path?>exercicis.php" style="">
                <span><img style="height: 30px;" src="img/run.png"> <?=' '.t('Exercicis');?></span>
            </a>
        </div>


        <div class="col-4 align-middle columna">
            <a class="btn btn-<?php if($titol == t('Entrenament')){ echo'primary';}else{echo'secondary';} ?> btn-sm" href="<?=$path?>index.php" style="">
                <span><img style="height: 30px;" src="img/time.png"> <?=' '.t('Entrenament');?></span>
            </a>
        </div>


        <div class="col-4 align-middle columna">
            <a class="btn btn-<?php if($titol == t('Perfil')){ echo'primary';}else{echo'secondary';} ?> btn-sm" href="<?=$path?>perfil.php" style="">
                <span><img style="height: 30px;" src="img/usuari.png"><?=' '.t('Perfil');?></span>
            </a>
        </div>

    </div>
</div>

