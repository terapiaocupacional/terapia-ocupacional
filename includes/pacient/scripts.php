<?php
/**
 * Created by PhpStorm.
 * User: ivanortega
 * Date: 11/03/2019
 * Time: 18:10
 */

$path = "../";
if(isset($relativePath)){
    $path = $relativePath;
}

?>
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>



<!-- Bootstrap core JavaScript-->
<script src="<?=$path?>vendor/jquery/jquery.min.js"></script>
<script src="<?=$path?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?=$path?>vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Page level plugin JavaScript-->
<script src="<?=$path?>vendor/chart.js/Chart.min.js"></script>
<script src="<?=$path?>vendor/datatables/jquery.dataTables.js"></script>
<script src="<?=$path?>vendor/datatables/dataTables.bootstrap4.js"></script>

<!-- Custom scripts for all pages-->
<script src="<?=$path?>js/sb-admin.min.js"></script>

<!-- Demo scripts for this page-->
<script src=".<?=$path?>js/demo/datatables-demo.js"></script>
<script src=".<?=$path?>js/demo/chart-area-demo.js"></script>

<script>
    $(document).ready(function() {
        $('table').DataTable();
    } );
</script>

<script>
    function onResizeReadMoreLayout(){
        var html = document.querySelector("#menuPacient");

        var viewportwidth = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;
        console.log(viewportwidth);
        if(viewportwidth >= 992){
            html.classList.remove("sticky-bottom");
            html.classList.remove("fixed-bottom");
            html.classList.add("sticky-top");
            html.classList.add("fixed-top");
        }else{
            html.classList.remove("sticky-top");
            html.classList.remove("fixed-top");
            html.classList.add("sticky-bottom");
            html.classList.add("fixed-bottom");
        }

    }


    // initial state
    onResizeReadMoreLayout();
    // on resize
    window.addEventListener("resize", onResizeReadMoreLayout);
</script>