<?php
/**
 * Created by PhpStorm.
 * User: ivanortega
 * Date: 02/05/2019
 * Time: 15:29
 */

if(!isset($titol)){
    $titol = "";
}
if(isset($relativePath)){
    $path = $relativePath;
}
if(isset($_SESSION['Codi_Idioma'])){
    setlocale(LC_TIME,$_SESSION['Codi_Idioma']);
}


$miFecha= time();
?>

<meta charset="UTF-8">
<title>Teràpia Ocupacional - <?=$titol?> </title>
<meta name="description" content="Treball Multimèdia UVIC conjuntament amb Teràpia Oucupacional" />
<link rel="shortcut icon" href="./favicon.ico" type="image/x-icon">
<link rel="icon" href="./favicon.ico" type="image/x-icon">


<!-- Custom fonts for this template-->
<link href="<?=$path?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

<!-- Page level plugin CSS-->
<link href="<?=$path?>vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

<!-- Custom styles for this template-->
<link href="<?=$path?>css/sb-admin.css" rel="stylesheet">
<link href="<?=$path?>css/general.css" rel="stylesheet">

<style>
    html{
        padding: 0;
        margin: 0;
        background-color: #5f5f5f;
    }
    body{
        background-color: #5f5f5f;
    }
</style>