<?php

@ob_start();
session_start();

$servername = "localhost";
$usr = "root";
$psw = "";
$dbname = "terapia_db";
$idioma = 1;

if(isset($_SESSION['Id_Idioma']) && $_SESSION['Id_Idioma'] > 0){
    $idioma = $_SESSION['Id_Idioma'];
}


// Create connection

$conn = new mysqli($servername, $usr, $psw, $dbname,3306);
if (!$conn->set_charset("utf8")) {
    printf("Error cargando el conjunto de caracteres utf8: %s\n", $conn->error);
    exit();
}