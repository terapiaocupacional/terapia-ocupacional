<?php
/**
 * Created by PhpStorm.
 * User: ivanortega
 * Date: 11/03/2019
 * Time: 17:44
 */

include_once("../includes/includes.php");

$title = t("Usuaris");

if(isset($_GET['edit'])){

}

if(isset($_GET['delete'])){
    $sql = "DELETE FROM usuaris WHERE Username like '".$_GET['delete']."'";
    $conn->query($sql);
    header('Location:'.$_SERVER['PHP_SELF'] );
}

if(isset($_POST['uname'])){
    $uname = mysqli_real_escape_string($conn, $_POST['uname']);
    $name = mysqli_real_escape_string($conn, $_POST['name']);
    $idIdioma = mysqli_real_escape_string($conn, $_POST['idIdioma']);
    $password = mysqli_real_escape_string($conn, $_POST['password']);

    if(!isset($_POST['admin'])){
        $pAdmin = 0;
    }else{
        $pAdmin = 1;
    }

    $sql = "INSERT INTO usuaris (Username, Id_Idioma, Nom, Password, Admin) VALUES ('".$uname."',$idIdioma,'".$name."','".md5($password)."',".$pAdmin.")";
    $conn->query($sql);
    header('Location:'.$_SERVER['PHP_SELF'] );
}


?>

<!DOCTYPE html>
<html lang="ca">

<head>
<?php include_once("./content/header.php"); ?>


</head>


<body id="page-top">

<?php include_once("./content/nav.php");?>

  <div id="wrapper">

    <!-- Sidebar -->
    <?php include_once("./content/sidebar.php");?>

    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="./index.php"><?=t("Inici");?></a>
          </li>
          <li class="breadcrumb-item active"><?= $title?></li>
        </ol>
        <!-- Icon Cards-->

        <article>
            <h1><?=$title; ?></h1>

            <form action='#' method='post'>

                <p><label><?=t("Username");?></label><br />
                    <input class="form-control" type='text' name='uname' required></p>
                <p><label><?=t("Nom");?></label><br />
                    <input class="form-control" type='text' name='name' required></p>
                <p><label><?=t("Contrasenya");?></label><br />
                    <input class="form-control" type='password' name='password' required></p>
                <p>
                    <label for="inputState"><?=t("Idioma");?></label>
                    <select id="inputState" class="form-control" required name="idIdioma">
                        <option disabled><?=t('Selecciona una opció')?></option>
                        <?php OptionIdiomes(); ?>
                    </select>
                </p>
                <p>
                    <label for="nom"><?=t('Administrador')?></label>
                    <input id="nom" type="checkbox" class="" name='admin' >
                </p>
                <p><input type='submit' class="btn btn-primary" name='submit' value='<?=t("Desar");?>'></p>
            </form>

            <br>
            <hr />

            <h2><?=t('Llista Usuaris'); ?></h2><br>

            <div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-table"></i>
                    <?= $title?></div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th><?=t("Nom Usuari");?></th>
                                <th><?=t("Nom");?></th>
                                <th><?=t("Idioma");?></th>
                                <th><?=t("Administrador");?></th>
                                <th><?=t("Acció");?></th>
                            </tr>
                            </thead>

                            <tbody>
                                <?php llistaUsuaris(); ?>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </article>






        <?php include_once("./content/footer.php");?>
    </div>
  </div>
  <?php include_once("./content/scripts.php");?>

</body>

</html>
