<?php
/**
 * Created by PhpStorm.
 * User: ivanortega
 * Date: 11/03/2019
 * Time: 18:10
 */

$path = "../";
if(isset($relativePath)){
    $path = $relativePath;
}

?>
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>



<!-- Bootstrap core JavaScript-->
<script src="<?=$path?>vendor/jquery/jquery.min.js"></script>
<script src="<?=$path?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?=$path?>vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Page level plugin JavaScript-->
<script src="<?=$path?>vendor/chart.js/Chart.min.js"></script>
<script src="<?=$path?>vendor/datatables/jquery.dataTables.js"></script>
<script src="<?=$path?>vendor/datatables/dataTables.bootstrap4.js"></script>

<!-- Custom scripts for all pages-->
<script src="<?=$path?>js/sb-admin.min.js"></script>

<!-- Demo scripts for this page-->
<script src=".<?=$path?>js/demo/datatables-demo.js"></script>
<script src=".<?=$path?>js/demo/chart-area-demo.js"></script>

<script>
    $(document).ready(function() {
        $('table').DataTable();
    } );
</script>