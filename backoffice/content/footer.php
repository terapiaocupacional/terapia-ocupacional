<?php
/**
 * Created by PhpStorm.
 * User: ivanortega
 * Date: 11/03/2019
 * Time: 18:08
 */
?>

<!-- Sticky Footer -->
<footer class="sticky-footer">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright © UVIC 2019</span>
        </div>
    </div>
</footer>
