<?php
$path = "../";
if(isset($relativePath)){
    $path = $relativePath;
}
?>

<meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Treball Multimèdia UVIC conjuntament amb Teràpia Ocupacional">
  <meta name="author" content="">

  <title>UVIC - Multimèdia</title>

  <!-- Custom fonts for this template-->
  <link href="<?=$path?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  <link href="<?=$path?>vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?=$path?>css/sb-admin.css" rel="stylesheet">
  <link href="<?=$path?>css/general.css" rel="stylesheet">

    <link rel="shortcut icon" href="<?=$path?>favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?=$path?>favicon.ico" type="image/x-icon">