<?php
/**
 * Created by PhpStorm.
 * User: ivanortega
 * Date: 11/03/2019
 * Time: 18:07
 */
function llistaIdiomes(){
    $conn = $GLOBALS['conn'];
    $sql = "SELECT * FROM idioma where id <> 1";


    $result = mysqli_query($conn, $sql);
    while($row = $result->fetch_array())
    {
        echo '<a class="dropdown-item" href="translate.php?idioma='.$row['Id'].'">'.t($row['Nom']).'</a>';
    }
}
?>


<ul class="sidebar navbar-nav">
    <li class="nav-item active">
        <a class="nav-link" href="index.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span><?=t('Inici')?></span>
        </a>
    </li>

    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw  fa-dumbbell"></i>
            <span><?=t('Exercicis')?></span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">

            <h6 class="dropdown-header"><?=t('Configuració')?>:</h6>

            <a class="dropdown-item" href="categories.php"><?=t('Categories')?></a>
            <a class="dropdown-item" href="rutpac.php"><?=t('Rutines a Pacient')?></a>

            <div class="dropdown-divider"></div>

            <h6 class="dropdown-header"><?=t('Manteniment')?>:</h6>

            <a class="dropdown-item" href="exercicis.php"><?=t('Exercicis')?></a>
            <a class="dropdown-item" href="rutines.php"><?=t('Rutines')?></a>


        </div>
    </li>
    <li class="nav-item">

        <a class="nav-link" href="usuaris.php">
            <i class="fas fa-fw fa-users"></i>
            <span><?=t('Usuaris')?></span></a>
    </li>

    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw  fa-globe"></i>
            <span><?=t('Traduccions')?></span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">

            <?php llistaIdiomes(); ?>
        </div>
    </li>


</ul>