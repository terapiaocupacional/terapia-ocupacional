<?php
/**
 * Created by PhpStorm.
 * User: ivanortega
 * Date: 11/03/2019
 * Time: 17:44
 */

include_once("../includes/includes.php");

$title = t("Rutines");

if(isset($_GET['delete'])){
    $sql = "DELETE FROM rutina WHERE Id = ".$_GET['delete'];
    $conn->query($sql);
    header('Location:'.$_SERVER['PHP_SELF'] );
}
//if form has been submitted process it
if(isset($_POST['submit'])) {

    $_POST = array_map('stripslashes', $_POST);

    //collect form data
    extract($_POST);


    $stmt = $conn->prepare("INSERT INTO rutina(Nom, Descripcio) VALUES (?,?)") ;
    $stmt->bind_param('ss', $nom, $descripcio);
    $stmt->execute();
}


if(isset($_GET['edit'])){
   header("Location:rutineslin.php?id=".$_GET['edit']);
}
?>

<!DOCTYPE html>
<html lang="ca">

<head>
<?php include_once("./content/header.php"); ?>

</head>

<body id="page-top">

<?php include_once("./content/nav.php");?>

  <div id="wrapper">

    <!-- Sidebar -->
    <?php include_once("./content/sidebar.php");?>

    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="./index.php"><?=t("Inici");?></a>
          </li>
          <li class="breadcrumb-item active"><?= $title?></li>
        </ol>
        <!-- Icon Cards-->

            <h1><?=t('Rutines')?></h1>
            <br>

          <form action='' method='post'>

              <p><label><?=t("Nom");?></label><br />
                  <input class="form-control" type='text' name='nom' required></p>
              <p><label><?=t("Descripció");?></label><br />
                  <textarea class="form-control rounded-0" name='descripcio' rows="3" required></textarea>


              <p><input type='submit' class="btn btn-primary" name='submit' value='<?=t("Desar");?>'></p>
          </form>


          <br>
          <hr />

          <h2><?=t('Llista Rutines'); ?></h2><br>

          <div class="card mb-3">
              <div class="card-header">
                  <i class="fas fa-table"></i>
                  <?= $title?></div>
              <div class="card-body">
                  <div class="table-responsive">
                      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                          <thead>
                          <tr>
                              <th><?=t("Nom");?></th>
                              <th><?=t("Descripció");?></th>
                              <th><?=t("Exercicis");?></th>
                              <th><?=t("Acció");?></th>
                          </tr>
                          </thead>

                          <tbody>
                          <?php llistaRutines(); ?>

                          </tbody>
                      </table>
                  </div>
              </div>

          </div>


        <?php include_once("./content/footer.php");?>
    </div>
  </div>
  <?php include_once("./content/scripts.php");?>

</body>

</html>
