<?php
/**
 * Created by PhpStorm.
 * User: ivanortega
 * Date: 11/03/2019
 * Time: 17:44
 */

include_once("../includes/includes.php");

$title = t("Exercicis");


    if(isset($_GET['delete'])){
        $sql = "DELETE FROM exercici WHERE Id = ".$_GET['delete'];
        $conn->query($sql);
        header('Location:'.$_SERVER['PHP_SELF'] );
    }
	//if form has been submitted process it
	if(isset($_POST['submit'])) {

        $_POST = array_map('stripslashes', $_POST);

        //collect form data
        extract($_POST);


        $stmt = $conn->prepare("INSERT INTO exercici(Id_Categoria, Video, Nom, Descripcio, Material,DescripcioCurta) VALUES (?,?,?,?,?,?)") ;
        $stmt->bind_param('isssss', $categoria, $video,$nom, $descripcio, $material,$descripcioCurta);
        $stmt->execute();
    }




?>

<!DOCTYPE html>
<html lang="ca">

<head>
<?php include_once("./content/header.php"); ?>


</head>

<body id="page-top">

<?php include_once("./content/nav.php");?>

  <div id="wrapper">

    <!-- Sidebar -->
    <?php include_once("./content/sidebar.php");?>

    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="./index.php"><?=t("Inici");?></a>
          </li>
          <li class="breadcrumb-item active"><?= $title?></li>
        </ol>
          <h1><?= $title?></h1>

          <!-- Content-->
          <form action='' method='post'>

              <div class="form-row">
                  <div class="form-group col-md-6">
                      <label for="nom"><?=t('Nom')?></label>
                      <input id="nom" type="text" class="form-control" name='nom' required >
                  </div>
                  <div class="form-group col-md-6">
                      <label for="inputState"><?=t("Categoria");?></label>
                      <select id="inputState" class="form-control" required name="categoria">
                          <option disabled><?=t('Selecciona una opció')?></option>
                          <?php OptionCategories(); ?>
                      </select>
                  </div>
              </div>
              <p><label><?=t("Descripció Curta");?></label><br />
                  <textarea class="form-control rounded-0" name='descripcioCurta' rows="2" required></textarea>
              <p><label><?=t("Descripció");?></label><br />
                  <textarea class="form-control rounded-0" name='descripcio' rows="4" required></textarea>
              <p><label><?=t("Video");?></label><br />
                  <input class="form-control" type='text' name='video' required></p>
              <p><label><?=t("Material");?></label><br />
                  <input class="form-control" type='text' name='material' required></p>



              <p><input type='submit' class="btn btn-primary" name='submit' value='<?=t("Desar");?>'></p>
          </form>


          <br>
          <hr />

          <h2><?=t('Llista Exercicis'); ?></h2><br>

          <div class="card mb-3">
              <div class="card-header">
                  <i class="fas fa-table"></i>
                  <?= $title?></div>
              <div class="card-body">
                  <div class="table-responsive">
                      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                          <thead>
                          <tr>
                              <th><?=t("Nom");?></th>
                              <th><?=t("Descripció");?></th>
                              <th><?=t("Acció");?></th>
                          </tr>
                          </thead>

                          <tbody>
                          <?php llistaExercicis(); ?>

                          </tbody>
                      </table>
                  </div>
              </div>

          </div>


          <!-- Content-->




        <?php include_once("./content/footer.php");?>
    </div>
  </div>
  <?php include_once("./content/scripts.php");?>

</body>

</html>
