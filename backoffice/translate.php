<?php
/**
 * Created by PhpStorm.
 * User: ivanortega
 * Date: 11/03/2019
 * Time: 17:44
 */

include_once("../includes/includes.php");

$title = t("Traducció");
$_idioma = $_GET['idioma'];

if($_idioma == 1 || !isset($_GET['idioma'])){
    header("Location: index.php");
}
function existeix($id,$pIdioma){
    $conn = $GLOBALS['conn'];
    $sql = "SELECT * FROM traduccions WHERE Id = ".$id;
    $sql .= " AND Id_Idioma = " . $pIdioma;

    $result = mysqli_query($conn, $sql);
    while($row = $result->fetch_array())
    {
        return true;
    }
    return false;
}

//if form has been submitted process it
if(isset($_POST['submit'])) {

    //collect form data
    extract($_POST);
    $needle   = "nom";
    foreach (array_keys($_POST) as $valor){

        $haystack = $valor;

        if( strpos( $haystack, $needle ) !== false) {
            if($_POST[$valor] != ''){
                $id = str_replace($needle,"",$haystack);
                if(existeix($id, $_idioma)){
                    $sql = "Update traduccions SET traduccio = '".$_POST[$valor]."' WHERE Id = ".$id." AND Id_Idioma = ".$_POST['idioma'];
                    $stmt = $conn->query($sql);
                }else{
                    $vTranslate = mysqli_real_escape_string($conn, $_POST[$valor]);
                    $stmt = $conn->prepare("INSERT INTO traduccions(Id, Id_Idioma, Traduccio) VALUES(?,?,?)");
                    $stmt->bind_param('iis', $id, $_POST['idioma'], $vTranslate );
                    $stmt->execute();
                }
            }
        }
    }
}
function nomIdioma($pIdioma){
    $conn = $GLOBALS['conn'];
    $sql = "SELECT * FROM idioma WHERE Id = ".$pIdioma;


    $result = mysqli_query($conn, $sql);
    while($row = $result->fetch_array())
    {
        return(t($row['Nom']));
    }
    header("Location: index.php");
}

?>

<!DOCTYPE html>
<html lang="ca">

<head>
    <?php include_once("./content/header.php"); ?>

</head>

<body id="page-top">

<?php include_once("./content/nav.php");?>

<div id="wrapper">

    <!-- Sidebar -->
    <?php include_once("./content/sidebar.php");?>

    <div id="content-wrapper">

        <div class="container-fluid">

            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="./index.php"><?=t("Inici");?></a>
                </li>
                <li class="breadcrumb-item active"><?= $title?></li>
            </ol>
            <!-- Icon Cards-->

            <h1><?=$title?></h1>
            <br>

            <form action='' method='post'>

               <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="nomCat'.$row['id'].'"><?=t('Català')?></label>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="nom'.$row['id'].'"><?=nomIdioma($_idioma);?></label>
                    </div>
               </div>
                <?php
                    $conn = $GLOBALS['conn'];
                    $sql = "SELECT t.*, (SELECT Traduccio from traduccions p where p.Id = t.Id and Id_Idioma = ".$_idioma.") as tr FROM traduccions t WHERE id_idioma=1 Order by Traduccio asc";

                    $result = mysqli_query($conn, $sql);
                    while($row = $result->fetch_array())
                    {
                        echo'<div class="form-row">
                                <div class="form-group col-md-6">
                                    <input id="nomCat'.$row['Id'].'"  class="form-control" value="'.str_replace('"',"'",$row['Traduccio']).'" name="nomCat'.$row['Id'].'" disabled >
                                </div>';
                        echo'<div class="form-group col-md-6">
                                    <input id="nom'.$row['Id'].'"  class="form-control" value="'.str_replace('"',"'",$row['tr']).'" name="nom'.$row['Id'].'" >
                                </div>';
                        echo"</div>";

                    }
                ?>
                <input name="idioma" hidden value="<?=$_idioma?>">
                <p><input type='submit' class="btn btn-primary" name='submit' value='<?=t("Desar");?>'></p>
            </form>


            <br>

            <?php include_once("./content/footer.php");?>
        </div>
    </div>
    <?php include_once("./content/scripts.php");?>

</body>

</html>
