<?php
/**
 * Created by PhpStorm.
 * User: ivanortega
 * Date: 11/03/2019
 * Time: 17:44
 */

include_once("../includes/includes.php");

$title = t("Rutines a Pacient");

if(isset($_GET['edit'])){

}

if(isset($_GET['delete'])){
    $sql = "DELETE FROM wo_usr WHERE Id like '".$_GET['delete']."'";
    $conn->query($sql);
    header('Location:'.$_SERVER['PHP_SELF'] );
}

if(isset($_POST['Usuari']) && isset($_POST['Rutina'])){
    $_POST = array_map('stripslashes', $_POST);
    //collect form data
    extract($_POST);

    $sql = "INSERT INTO wo_usr (Id_Username, Id_Rutina, `Data`) VALUES ('".$Usuari."',$Rutina, '".$data."')";
    $conn->query($sql);
    //echo $sql;
    header('Location:'.$_SERVER['PHP_SELF'] );
}


?>

<!DOCTYPE html>
<html lang="ca">

<head>
<?php include_once("./content/header.php"); ?>


</head>


<body id="page-top">

<?php include_once("./content/nav.php");?>

  <div id="wrapper">

    <!-- Sidebar -->
    <?php include_once("./content/sidebar.php");?>

    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="./index.php"><?=t("Inici");?></a>
          </li>
          <li class="breadcrumb-item active"><?= $title?></li>
        </ol>
        <!-- Icon Cards-->

        <article>
            <h1><?=$title; ?></h1>

            <form action='#' method='post'>

                <p>
                    <label for="inputState"><?=t("Usuari");?></label>
                    <select id="inputState" class="form-control" required name="Usuari">
                        <option disabled><?=t('Selecciona una opció')?></option>
                        <?php OptionUsuaris(); ?>
                    </select>
                </p>
                <p>
                    <label for="inputState"><?=t("Rutina");?></label>
                    <select id="inputState" class="form-control" required name="Rutina">
                        <option disabled><?=t('Selecciona una opció')?></option>
                        <?php OptionRutines(); ?>
                    </select>
                </p>
                <p>
                    <label for="data"><?=t('Data')?></label>
                    <input id="data" type="date" class="form-control col-md-6" step="1" min="2019-01-01" max="2113-12-31" name='data' value="<?php echo date("Y-m-d");?>" >
                </p>
                <p><input type='submit' class="btn btn-primary" name='submit' value='<?=t("Desar");?>'></p>
            </form>

            <br>
            <hr />

            <h2><?=t('Llista Rutines a Pacients'); ?></h2><br>

            <div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-table"></i>
                    <?= $title?></div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th><?=t("Rutina");?></th>
                                <th><?=t("Usuari");?></th>
                                <th><?=t("Data");?></th>
                                <th><?=t("Acció");?></th>
                            </tr>
                            </thead>

                            <tbody>
                                <?php llistaRutinesAPacients(); ?>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </article>






        <?php include_once("./content/footer.php");?>
    </div>
  </div>
  <?php include_once("./content/scripts.php");?>

</body>

</html>
