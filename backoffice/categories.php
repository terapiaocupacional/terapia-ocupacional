<?php
/**
 * Created by PhpStorm.
 * User: ivanortega
 * Date: 11/03/2019
 * Time: 17:44
 */

include_once("../includes/includes.php");

$title = t("Categories");

if(isset($_GET['edit'])){

}

if(isset($_GET['delete'])){
    $sql = "DELETE FROM categoria WHERE Id = ".$_GET['delete'];
    $conn->query($sql);
    header('Location:'.$_SERVER['PHP_SELF'] );
}

if(isset($_POST['name'])){
    $name = mysqli_real_escape_string($conn, $_POST['name']);
    $desc = mysqli_real_escape_string($conn, $_POST['description']);
    $img = mysqli_real_escape_string($conn, $_POST['image']);

    $sql = "INSERT INTO categoria (Nom, Descripcio,Imatge) VALUES ('".$name."','".$desc."', '".$img."')";
    //echo($sql);
    $conn->query($sql);
    //header('Location:'.$_SERVER['PHP_SELF'] );
}



?>

<!DOCTYPE html>
<html lang="ca">

<head>
<?php include_once("./content/header.php"); ?>


</head>

<body id="page-top">

<?php include_once("./content/nav.php");?>

  <div id="wrapper">

    <!-- Sidebar -->
    <?php include_once("./content/sidebar.php");?>

    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="./index.php"><?=t("Inici");?></a>
          </li>
          <li class="breadcrumb-item active"><?= $title?></li>
        </ol>
        <!-- Icon Cards-->

        <article>
            <h1><?=$title; ?></h1>

            <form action='' method='post'>

                <p><label><?=t("Nom");?></label><br />
                    <input class="form-control" type='text' name='name' required></p>

                <p><label><?=t("Descripció");?></label><br />
                    <textarea class="form-control rounded-0" name='description' rows="3" required></textarea>

                <p><label><?=t("Imatge");?></label><br />
                    <input class="form-control" type='url' name='image' required></p>

                <p><input type='submit' class="btn btn-primary" name='submit' value='<?=t("Desar");?>'></p>
            </form>

            <br>
            <hr />

            <h2><?=t('Llista Categories'); ?></h2><br>

            <div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-table"></i>
                    <?= $title?></div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th><?=t("Nom");?></th>
                                <th><?=t("Descripció");?></th>
                                <th><?=t("Imatge")?></th>
                                <th><?=t("Acció");?></th>
                            </tr>
                            </thead>

                            <tbody>
                                <?php llistaCategories(); ?>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </article>






        <?php include_once("./content/footer.php");?>
    </div>
  </div>
  <?php include_once("./content/scripts.php");?>

</body>

</html>
