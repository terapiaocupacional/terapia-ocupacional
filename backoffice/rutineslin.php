<?php
/**
 * Created by PhpStorm.
 * User: ivanortega
 * Date: 11/03/2019
 * Time: 17:44
 */

include_once("../includes/includes.php");

$title = t("Línia Rutines");
$nomRutina = '';

if(!isset($_GET['id'])){
    header('Location: rutines.php' );
}else{
    $conn = $GLOBALS['conn'];
    $sql = "SELECT Nom FROM rutina WHERE Id= ".$_GET['id'];
    $result = mysqli_query($conn, $sql);
    while($row = $result->fetch_array()){
        $nomRutina = $row['Nom'];
    }

}


if(isset($_GET['delete'])){
    $sql = "DELETE FROM rutineslin WHERE Id_Rutina = ".$_GET['delete']." and Linia = ".$_GET['lin'];
    $conn->query($sql);
    updateLinies();
    header('Location:'.$_SERVER['PHP_SELF']."?id=".$_GET['id']);
}

//if form has been submitted process it
if(isset($_POST['submit']) && isset($_POST['exercici'])) {

    $_POST = array_map('stripslashes', $_POST);

    //collect form data
    extract($_POST);
    $t = date("00:00:00");
    $r = 0;
    $l = 999;
    $ex = $exercici;
    if($tipus == 'tTemps'){
        $t = date('H:i:s',strtotime('+'.$valor.' seconds',strtotime($t)));
    }else{
        $r = $valor;
    }

    $stmt = $conn->prepare("INSERT INTO rutineslin(Id_Rutina,Linia, Repeticions, Temps, Id_Exercici) VALUES (?, ?, ?,?, ?)") ;
    $stmt->bind_param('iiisi',$_GET['id'] , $l ,$r, $t,$ex);
    $stmt->execute();
    updateLinies();
}


function updateLinies(){
    $conn = $GLOBALS['conn'];
    $sql = "SELECT * FROM rutineslin WHERE Id_Rutina = ".$_GET['id']." Order by Linia asc";

    $count = 1;
    $result = mysqli_query($conn, $sql);
    while($row = $result->fetch_array())
    {
        $stmt2 = $conn->prepare("UPDATE rutineslin SET Linia = ?  WHERE Id_Rutina = ".$row["Id_Rutina"]." AND Linia = ".$row['Linia']) ;
        $stmt2->bind_param('i', $count);
        $stmt2->execute();
        $count += 1;
    }


}
?>

<!DOCTYPE html>
<html lang="ca">

<head>
    <?php include_once("./content/header.php"); ?>

</head>

<body id="page-top">

<?php include_once("./content/nav.php");?>

<div id="wrapper">

    <!-- Sidebar -->
    <?php include_once("./content/sidebar.php");?>

    <div id="content-wrapper">

        <div class="container-fluid">

            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="./index.php"><?=t("Inici");?></a>
                </li>
                <li class="breadcrumb-item">
                    <a href="./rutines.php"><?=t("Rutines");?></a>
                </li>
                <li class="breadcrumb-item active"><?= $title?></li>
            </ol>
            <!-- Icon Cards-->

            <h1><?=t('Rutines')." - ".$nomRutina?></h1>
            <br>

            <form action='' method='post'>
                <label for="inputState"><b><?=t("Exercici");?></b></label>
                <div class="form-group col-md-6">

                    <select id="inputState" class="form-control" required name="exercici">
                        <option disabled><?=t('Selecciona una opció')?></option>
                        <?php OptionExercicis(); ?>
                    </select>
                </div>
                <p><b><?=t("Tipus de control")?></b></p>
                <div class="form-group col-md-3">
                    <input type="radio" name="tipus" value="tTemps"><?=t("Temps (En Segons)")?><br>
                    <input type="radio" name="tipus" value="tRepeticions" checked><?=t("Repeticions")?><br>
                </div>
                <p><b><label><?=t("Valor");?></label></b></p>
                <div class="form-group col-md-3">

                        <input class="form-control" type='number' name='valor' required>
                </div>





                <p><input type='submit' class="btn btn-primary" name='submit' value='<?=t("Desar");?>'></p>
            </form>


            <br>
            <hr />

            <h2><?=t('Detall').' '.t($nomRutina); ?></h2><br>

            <div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-table"></i>
                    <?= $title?></div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th><?=t("Linia");?></th>
                                <th><?=t("Exercici")?></th>
                                <th><?=t("Repeticions");?></th>
                                <th><?=t("Temps");?></th>
                                <th><?=t("Acció");?></th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php llistaRutinesLinies(); ?>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>


            <?php include_once("./content/footer.php");?>
        </div>
    </div>
    <?php include_once("./content/scripts.php");?>

</body>

</html>
