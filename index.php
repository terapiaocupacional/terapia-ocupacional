<?php
/**
 * Created by PhpStorm.
 * User: ivanortega
 * Date: 20/03/2019
 * Time: 15:48
 */
//session_start();

$relativePath = "./";
include_once ('./includes/includes.php');
$titol = t("Entrenament");
$videoShow = false;

if(!isset($_SESSION['admin'])) {
    header("Location: backoffice/index.php");
}else if($_SESSION['admin'] == '1'){
    header("Location: backoffice/index.php");
}


//$exerciciDiari = getExercicis($rutinaDiaria);//Agafa els exercicis del dia corresponent a la o les id's de les rutines

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    include_once('./includes/pacient/header.php');
    ?>

    <style>
        #myVideo {
            position: fixed;
            left: 50%;
            top: 50%;
            width: 100%;
            min-width: 100%;
            z-index: 9999;
            transform: translate(-50%,-50%);
        }

        /* Add some content at the bottom of the video/page */
        .content2 {
            position: fixed;
            bottom: 0;
            background: rgba(0, 0, 0, 0.5);
            color: #f1f1f1;
            width: 100%;
            padding: 20px;
            z-index: 10000;
        }

        /* Style the button used to pause/play the video */
        #myBtn {
            width: 200px;
            font-size: 18px;
            padding: 10px;
            border: none;
            background: #000;
            color: #fff;
            cursor: pointer;
        }

        #myBtn:hover {
            background: #ddd;
            color: black;
        }
    </style>
</head>
<body>

    <header>
        <h1 style="text-transform: capitalize;"><?=$_SESSION['nom'];?></h1>
        <data value="02/05/2019"><?=strftime("%A, %d de %B de %Y", $miFecha)?></data>
    </header>
    <!--/////////////////// Menu ////////////////////////////-->
    <?php include_once('./includes/pacient/menu.php'); ?>
    <!--/////////////////// Menu ////////////////////////////-->

    <div class="contenidor">
        <?php
        if(isset($_GET['Id'])){
            //$sql = "UPDATE wo_usr SET Fet = 1 WHERE Id = ".$_GET['Id'];
            //$conn->query($sql);
            //header('Location:'.$_SERVER['PHP_SELF'] );
            $videoShow = true;
        }
        if(isset($_GET['complete'])){
            $videoShow = false;
            $sql = "UPDATE wo_usr SET Fet = 1 WHERE Id = ".$_GET['Id'];
            $conn->query($sql);
            header('Location:'.$_SERVER['PHP_SELF'] );
        }
        getRutines($_SESSION['username']);
        ?>

    </div>
    <div style="z-index: 10001; background-color: #1b1e21; position: fixed; min-width: 100%; min-height: 100%;top:0;left:0;" <?php if(!$videoShow){echo'hidden';}?>>
        <img src="img/loading.gif" style="height: 200px; position: fixed;top:50%;left:50%;transform: translate(-50%,-50%);">
        <video autoplay muted loop id="myVideo" >
            <source src="videos/video1R.mp4" type="video/mp4">
        </video>

        <!-- Optional: some overlay text to describe the video -->
        <div class="content2">
            <h1>Heading</h1>
            <p>Lorem ipsum...</p>
            <!-- Use a button to pause/play the video with JavaScript -->
            <button id="myBtn" onclick="videoPause()"><?=t("Pausa")?></button>
            <button id="myBtn" onclick="complete()">Complete</button>
        </div>
    </div>


    <?php include_once ('./includes/pacient/scripts.php'); ?>

    <script>
        // Get the video
        var video = document.getElementById("myVideo");

        // Get the button
        var btn = document.getElementById("myBtn");

        // Pause and play the video, and change the button text
        function videoPause() {
            if (video.paused) {
                video.play();
                btn.innerHTML = '<?=(t("Pausa"))?>';
            } else {
                video.pause();
                btn.innerHTML = '<?=t("Play")?>';
            }
        }
        function complete() {
            window.location.href = document.URL + '&complete=true';
        }
    </script>
</body>
</html>
