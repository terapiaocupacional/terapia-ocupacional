<?php
/*Edgar Santamaria
 * 20/03/2019
 * Categories
 */
$relativePath = "./";
include_once ('includes/includes.php');

$titol = t("Exercicis");

//retorna el resultat de la select de categories per omplir el frontend_categories
function getCategoria(){
    $conn = $GLOBALS['conn'];
    $sql = "SELECT * from categoria";
    $result = mysqli_query($conn, $sql);
    return $result;
}

function omplirCategoria($idCat) {
    $conn = $GLOBALS['conn'];
    $sql = "SELECT * FROM exercici WHERE Id_Categoria=".$idCat;
    $oC = mysqli_query($conn, $sql);
    return $oC;
}

?>
<html>
    <head>
        <?php include_once('includes/pacient/header.php');?>

    </head>
    <body>
        <header>
            <h1><?=$titol?></h1>
            <data value="02/05/2019"><?=strftime("%A, %d de %B de %Y", $miFecha)?></data>
        </header>
        <!--/////////////////// Menu ////////////////////////////-->
        <?php include_once('./includes/pacient/menu.php'); ?>
        <!--/////////////////// Menu ////////////////////////////-->
        <?php

        if(isset($_GET['Id'])){
            ?>
            <div class="contenidor">
                <div class="container categories">
            <?php
            $arrCat = omplirCategoria($_GET['Id']);
            echo '<div class="row">';

            while($row = $arrCat->fetch_array())
            {
                //$bg = "background:  linear-gradient(to right, rgba(0,0,0,1) 0%,rgba(0,0,0,0) 100%), url('".$row['Imatge']."') no-repeat center; background-size: cover; ";

                echo '<div class="col-6 col-lg-4 bgCol" >';
                echo '<div class="columna" style="">';
                echo '<a class="btn-sm" href="'.$_SERVER['PHP_SELF'].'?ex='.$row['Id'].'" style="">';

                echo '</a>';
                echo '</div>';
                echo '<span><h2>'.t($row['Nom']).'</h2><p>'.t($row['DescripcioCurta']).'</p></span>';
                echo '</div>';

            }
            echo '</div>';
            ?>
                </div>
            </div>
            <?php
        }else{
            ?>
            <div class="contenidor">
                <div class="container categories">
                    <?php
                    $arrCat = getCategoria();
                    echo '<div class="row">';

                    while($row = $arrCat->fetch_array())
                    {

                        $bg = " background:  linear-gradient(to right, rgba(0,0,0,1) 0%,rgba(0,0,0,0) 100%), url('".$row['Imatge']."') no-repeat center; background-size: cover;";
                        $bg .= " transition: all 0.7s ease;";
                        echo '<div class="col-12 col-lg-6 bgCol" >';
                            echo '<div class="columna" style="'.$bg.'">';
                            echo '<a class="btn-sm" href="'.$_SERVER['PHP_SELF'].'?Id='.$row['Id'].'" style="">';

                            echo '</a>';
                            echo '</div>';
                        echo '<span><h2>'.t($row['Nom']).'</h2><p>'.t($row['Descripcio']).'</p></span>';
                        echo '</div>';

                    }
                    echo '</div>';
                    ?>

                </div>
            </div>
                <?php
            }
            ?>




        <?php include_once ('./includes/pacient/scripts.php'); ?>
    </body>
</html>
<?php

?>