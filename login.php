<?php
$relativePath = "./";
// connect to the database
include_once("./includes/functions.php");

// initializing variables
$username = "";
$email    = "";
$errors = array();

// LOGIN USER
if (isset($_POST['login_user'])) {
    $username = mysqli_real_escape_string($conn, $_POST['username']);
    $password = mysqli_real_escape_string($conn, $_POST['password']);

    if (empty($username)) {
        array_push($errors, "Username is required");
    }
    if (empty($password)) {
        array_push($errors, "Password is required");
    }

    if (count($errors) == 0) {
        $password = md5($password);
        $query = "SELECT u.*, i.Code FROM usuaris u left join idioma i on u.Id_Idioma = i.Id WHERE u.Username='".$username."' AND u.Password='".$password."'";
        $results = mysqli_query($conn, $query);
        if (mysqli_num_rows($results) == 1) {
            $_SESSION['username'] = $username;

            $_SESSION['success'] = "You are now logged in";
            if($row = $results->fetch_array()){
                $_SESSION['admin'] = $row["Admin"];
                $_SESSION['Id_Idioma'] = $row['Id_Idioma'];
                $_SESSION['nom'] = $row["Nom"];
                $_SESSION['Codi_Idioma'] = $row["Code"];
            }
            if ($_SESSION['admin'] == 1){
                header('location: backoffice/index.php');
            } else {
                echo 'teeeest';
                header('location: exercicis.php');
            }
            echo 'teeeest2';
                
        }else {
            array_push($errors, "El nom d'usuari o la contrasenya son incorrectes.");
        }
    }
}


?>


<!DOCTYPE html>
<html lang="en">

<head>
    <?php include_once("backoffice/content/header.php"); ?>
</head>

<body class="bg-dark" style="font-size: 1rem!important;">
<div class="container">
    <div class="card card-login mx-auto mt-5">
        <div class="card-header">Login</div>
        <div class="card-body">
            <form method="post" action="login.php">
                <?php include('backoffice/content/errors.php'); ?>
                <div class="form-group">
                    <label for="exampleInputEmail1">Username</label>
                    <input class="form-control"  type="text"  name="username">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input class="form-control"  type="password" name="password">
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox"> Remember Password</label>
                    </div>
                </div>


                <button type="submit" class="btn btn-primary btn-block" name="login_user">Login</button>
            </form>
            <!-- <div class="text-center">
                <a class="d-block small mt-3" href="register.php">Register an Account</a>
                <!--   <a class="d-block small" href="forgot-password.php">Forgot Password?</a>
            </div> -->
        </div>
    </div>
</div>
<?php include_once("backoffice/content/scripts.php");?>
</body>

</html>
