<?php
/*Edgar Santamaria
 * 20/03/2019
 * Categories
 */
$relativePath = "./";
include_once ('includes/includes.php    ');
$titol = t("Categories");
//retorna el resultat de la select de categories per omplir el frontend_categories
function getIdiomes(){
    $conn = $GLOBALS['conn'];
    $sql = "SELECT * from idioma";
    $result = mysqli_query($conn, $sql);
    return $result;
}
function getInfoUsuari() {
    $conn = $GLOBALS['conn'];
    $sql = "SELECT u.*, i.Img FROM usuaris u left join idioma i u.Id_Idioma = i.Id WHERE Username='".$_SESSION['username']."'";
    $getIU = mysqli_query($conn, $sql);
    return $getIU;
}
?>
    <html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title><!--TITOL--></title>

        <!-- Css -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
        <!-- Google icons -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    </head>
    <body>
    <?php
    $arrIU = getInfoUsuari();
    $nom = $user = $privilegi = $ididioma = '';

    while ($row = $arrIU->fetch_array()){
        $nom = $row['Nom'];
        $user = $row['Username'];
        $ididioma = $row['Id_Idioma'];
        $privilegi = $row['Admin'];
        $src = $row['Img'];
    }

    ?>

    <?php

    echo '<img src="'.$src.'" alt="" id="menu-idioma" />'.$row['Nom'];

    echo '<div class="container">';
    echo '<div class="row">';
    echo '<div class="col-12 col-md-12">';
    echo '<h2>'.$nom.'</h2>';
    echo '</div>';
    echo '<div class="separador col-12 col-md-12"></div>';
    echo '</div>';
    echo '</div>';

    ?>
    <form action="updateIdioma.php" method="get">
        <input type="text" name="nom_usuari" class="inputs-usuari">
        <input type="password" name="psw" class="inputs-usuari">
        <input list="idiomes" name="idioma">
        <datalist id="idiomes">
            <?php
            while($row = $arrIdiomes->fetch_array()){
                echo '<option value="'.$row['Nom'].'">';
            }
            ?>
        </datalist>
        <input type="submit">
    </form>
    <footer class="container" style="background: #0a0f44; position: fixed; bottom: 0px; max-width: 100%; ">
        <div class="row text-center col-white centrar-tot">
            <div class="col-4 col-md-4 pointer" >
                <p class="centrar-tot"><i class="material-icons centrar-tot">account_circle</i>Categorias</p>
            </div>
            <div class="col-4 col-md-4 pointer">
                <p><i class="material-icons">accessible_forward</i>Activitat diaria</p>
            </div>
            <div class="col-4 col-md-4 pointer" style="background-color: #1d3e7b; border-radius: 50px">
                <p><i class="material-icons centrar-tot">account_circle</i>Perfil</p>
            </div>
        </div>
    </footer>
    <!-- JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
    </html>
<?php
$pageContents = ob_get_contents ();
ob_end_clean ();
echo str_replace ('<!--TITOL-->', $titol, $pageContents);
?>