<?php
/**
 * Created by PhpStorm.
 * User: ivanortega
 * Date: 20/03/2019
 * Time: 15:48
 */
//session_start();

$relativePath = "./";
include_once ('./includes/includes.php');
$titol = t("Perfil");


if(!isset($_SESSION['admin'])) {
    header("Location: backoffice/index.php");
}else if($_SESSION['admin'] == '1'){
    header("Location: backoffice/index.php");
}else{
    //header("Location: frontend_categories.php");
}

if(isset($_POST['nom_usuari'])){
    $name = mysqli_real_escape_string($conn, $_POST['nom_usuari']);
    $sql = "UPDATE usuaris Set Nom='".$name."', Password = '".md5($_POST['psw'])."', Id_Idioma = ".$_POST['idioma']." WHERE Username = '".$_SESSION['username']."'";
    $_SESSION['nom'] = $name;
    header('Location:'.$_SERVER['PHP_SELF']);
}

function getIdiomes(){
    $conn = $GLOBALS['conn'];
    $sql = "SELECT * from idioma";
    $result = mysqli_query($conn, $sql);
    return $result;
}
function getInfoUsuari() {
    $conn = $GLOBALS['conn'];
    $sql = "SELECT u.*, i.Img FROM usuaris u left join idioma i on u.Id_Idioma = i.Id WHERE Username='".$_SESSION['username']."'";
    $getIU = mysqli_query($conn, $sql);
    return $getIU;
}


?>
<!DOCTYPE html>
<html lang="es">
<head>
    <?php
    include_once('./includes/pacient/header.php');
    ?>
    <style>
        form{
            background-color: whitesmoke;
            padding:20px;
            border-radius:20px;
            margin: 2em;
            box-shadow: 10px 10px 10px 0 rgb(42, 41, 41);
        }
        .buttons .row{
            flex-wrap:initial;
        }
        .buttons {
            margin:0;
        }
        @media (min-width: 768px){
            .container {
                max-width: 100%;
            }
        }


    </style>
</head>
<body>

    <header>
        <h1 style="text-transform: capitalize;"><?=$_SESSION['nom'];?></h1>
        <data value="02/05/2019"><?=strftime("%A, %d de %B de %Y", $miFecha)?></data>
    </header>
    <!--/////////////////// Menu ////////////////////////////-->
    <?php include_once('./includes/pacient/menu.php'); ?>
    <!--/////////////////// Menu ////////////////////////////-->

    <div class="contenidor">
        <?php
        $arrIU = getInfoUsuari();
        $nom = $user = $privilegi = $ididioma = '';

        while ($row = $arrIU->fetch_array()){
            $nom = $row['Nom'];
            $user = $row['Username'];
            $ididioma = $row['Id_Idioma'];
            $privilegi = $row['Admin'];
            $src = $row['Img'];
        }

        ?>

        <form action='#' method='post'>
            <div class="form-group">
                <label for="name"><?=t('Nom')?></label>
                <input type="text" class="form-control" name="nom_usuari" id="name" aria-describedby="emailHelp" required placeholder="<?=$_SESSION['nom']?>">
                <small id="emailHelp" class="form-text text-muted"><?=t('No es modificara el username')?></small>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1"><?=t('Contrasenya')?></label>
                <input type="password" class="form-control" id="exampleInputPassword1" name="psw" required placeholder="<?=t('Contrasenya')?>">
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1"><?=t('Idioma')?></label>
                <select  name="idioma" class="form-control" id="exampleFormControlSelect1">
                    <?php
                    $arrIU = getIdiomes();
                    while($row = $arrIU->fetch_array()){
                        echo '<option value="'.$row['Id'].'">'.$row['Nom'].'</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="container buttons">
                <div class="row">
                    <div class="col-sm">
                        <button  type="submit" class="btn btn-lg btn-primary"><?=t('Desar')?></button>
                    </div>
                    <div class="col-sm">
                        <a href="backoffice/content/logout.php" class="btn btn-lg btn-danger float-right"><?=t('Sortir')?></a>
                    </div>
                </div>
            </div>


        </form>


    </div>

    <?php include_once ('./includes/pacient/scripts.php'); ?>
</body>
</html>
