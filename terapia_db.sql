-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 25-03-2019 a las 17:44:18
-- Versión del servidor: 10.1.33-MariaDB
-- Versión de PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `terapia_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `Id` int(11) NOT NULL,
  `Nom` text NOT NULL,
  `Descripcio` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`Id`, `Nom`, `Descripcio`) VALUES
(1, 'Cat1', 'Test categoria 1'),
(4, 'Cat2', 'Test Categoria 2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `exercici`
--

CREATE TABLE `exercici` (
  `Id` int(11) NOT NULL,
  `Id_Categoria` int(11) NOT NULL,
  `Video` text NOT NULL,
  `Nom` text NOT NULL,
  `Descripcio` text NOT NULL,
  `Material` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `exercici`
--

INSERT INTO `exercici` (`Id`, `Id_Categoria`, `Video`, `Nom`, `Descripcio`, `Material`) VALUES
(1, 1, '', 'Ex 1', 'Desc ex 1', 'Material Necesari'),
(2, 1, '', 'Ex 2', 'Desc Ex 2', 'Material ex 2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idioma`
--

CREATE TABLE `idioma` (
  `Id` int(11) NOT NULL,
  `Nom` text NOT NULL,
  `Img` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `idioma`
--

INSERT INTO `idioma` (`Id`, `Nom`, `Img`) VALUES
(1, 'Catala', ''),
(2, 'Castella', ''),
(3, 'Angles', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resultat`
--

CREATE TABLE `resultat` (
  `Id` int(11) NOT NULL,
  `Id_Wo_Usr` int(11) NOT NULL,
  `Id_Exercici` int(11) NOT NULL,
  `Data` date NOT NULL,
  `Valoracio` int(11) NOT NULL,
  `Comentari` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rutina`
--

CREATE TABLE `rutina` (
  `Id` int(11) NOT NULL,
  `Nom` text NOT NULL,
  `Descripcio` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rutina`
--

INSERT INTO `rutina` (`Id`, `Nom`, `Descripcio`) VALUES
(1, 'Rutina1', 'Descripcio rutina 1'),
(3, 'Rutina 2', 'Descripció rutina 2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rutineslin`
--

CREATE TABLE `rutineslin` (
  `Id_Rutina` int(11) NOT NULL,
  `Linia` int(11) NOT NULL,
  `Id_Exercici` int(11) NOT NULL,
  `Repeticions` int(11) NOT NULL,
  `Temps` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rutineslin`
--

INSERT INTO `rutineslin` (`Id_Rutina`, `Linia`, `Id_Exercici`, `Repeticions`, `Temps`) VALUES
(1, 1, 1, 40, '00:07:00'),
(1, 2, 1, 4, '00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `traduccions`
--

CREATE TABLE `traduccions` (
  `Id` int(11) NOT NULL,
  `Id_Idioma` int(11) NOT NULL,
  `Traduccio` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `traduccions`
--

INSERT INTO `traduccions` (`Id`, `Id_Idioma`, `Traduccio`) VALUES
(2, 2, 'Esp Test'),
(3, 2, 'Ejercicios'),
(28, 1, 'Exercicis'),
(28, 2, 'Ejercicios'),
(29, 1, 'Configuració'),
(29, 2, 'Configuración'),
(30, 1, 'Categories'),
(30, 2, 'Categorias'),
(31, 1, 'Rutines a Pacient'),
(31, 2, 'Rutinas a Paciente'),
(32, 1, 'Manteniment'),
(32, 2, 'Mantenimiento'),
(33, 1, 'Rutines'),
(33, 2, 'Rutinas'),
(34, 1, 'Inici'),
(34, 2, 'Inicio'),
(35, 1, 'Català'),
(35, 2, 'Catalan'),
(36, 1, 'Desar'),
(36, 2, 'Guardar'),
(37, 1, 'Traducció'),
(37, 2, 'Traducción'),
(38, 1, 'Blank'),
(38, 2, 'Blanco'),
(39, 1, 'Terapia Ocupacional'),
(39, 2, 'Terapia Ocupacional'),
(40, 1, 'Nom'),
(40, 2, 'Nombre'),
(41, 1, 'Descripció'),
(41, 2, 'Descripción'),
(42, 1, 'Llista Categories'),
(42, 2, 'Lista Categorías'),
(43, 1, 'Acció'),
(43, 2, 'Acción'),
(44, 1, 'Categoria'),
(44, 2, 'Categoría'),
(45, 1, 'Selecciona una opció'),
(45, 2, 'Selecciona una opción'),
(46, 1, 'Video'),
(46, 2, 'Vídeo'),
(47, 1, 'Material'),
(47, 2, 'Material'),
(48, 1, 'Llista Exercicis'),
(48, 2, 'Lista Ejercicios'),
(49, 1, 'Llista Rutines'),
(49, 2, 'Lista Rutinas'),
(50, 1, 'Traduccions'),
(50, 2, 'Traducciones'),
(51, 1, 'Castella'),
(51, 2, 'Castellano'),
(52, 1, 'Angles'),
(52, 2, 'Ingles'),
(55, 1, 'Sortir'),
(55, 2, 'Salir'),
(56, 1, 'Segur que vols sortir?'),
(56, 2, 'Seguro que quieres salir?'),
(57, 1, 'Seleccioneu \"Sortir\" a continuació si esteu a segurs de tancar la vostra sessió actual.'),
(57, 2, 'Seleccione \"Salir\" a continuación si desea mantener seguros de cerrar su sesión actual.'),
(58, 1, 'Cancel·lar'),
(58, 2, 'Cancelar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuaris`
--

CREATE TABLE `usuaris` (
  `Username` varchar(20) NOT NULL,
  `Id_Idioma` int(11) NOT NULL,
  `Nom` text NOT NULL,
  `Password` text NOT NULL,
  `Admin` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuaris`
--

INSERT INTO `usuaris` (`Username`, `Id_Idioma`, `Nom`, `Password`, `Admin`) VALUES
('admin', 2, 'Administrador', '81dc9bdb52d04dc20036dbd8313ed055', b'1'),
('ivan.ortega', 1, 'Ivan', '81dc9bdb52d04dc20036dbd8313ed055', b'1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wo_usr`
--

CREATE TABLE `wo_usr` (
  `Id` int(11) NOT NULL,
  `Id_Username` varchar(20) NOT NULL,
  `Id_Rutina` int(11) NOT NULL,
  `Data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `exercici`
--
ALTER TABLE `exercici`
  ADD PRIMARY KEY (`Id`,`Id_Categoria`),
  ADD KEY `Id_Categoria` (`Id_Categoria`);

--
-- Indices de la tabla `idioma`
--
ALTER TABLE `idioma`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `resultat`
--
ALTER TABLE `resultat`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Id_Wo_Usr` (`Id_Wo_Usr`),
  ADD KEY `Id_Exercici` (`Id_Exercici`);

--
-- Indices de la tabla `rutina`
--
ALTER TABLE `rutina`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `rutineslin`
--
ALTER TABLE `rutineslin`
  ADD PRIMARY KEY (`Id_Rutina`,`Linia`),
  ADD KEY `Id_Exercici` (`Id_Exercici`);

--
-- Indices de la tabla `traduccions`
--
ALTER TABLE `traduccions`
  ADD PRIMARY KEY (`Id`,`Id_Idioma`),
  ADD KEY `Id_Idioma` (`Id_Idioma`);

--
-- Indices de la tabla `usuaris`
--
ALTER TABLE `usuaris`
  ADD PRIMARY KEY (`Username`),
  ADD KEY `Id_Idioma` (`Id_Idioma`);

--
-- Indices de la tabla `wo_usr`
--
ALTER TABLE `wo_usr`
  ADD PRIMARY KEY (`Id`,`Id_Username`,`Id_Rutina`),
  ADD KEY `Id_Username` (`Id_Username`),
  ADD KEY `Id_Rutina` (`Id_Rutina`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `exercici`
--
ALTER TABLE `exercici`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `idioma`
--
ALTER TABLE `idioma`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `resultat`
--
ALTER TABLE `resultat`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `rutina`
--
ALTER TABLE `rutina`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `wo_usr`
--
ALTER TABLE `wo_usr`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `exercici`
--
ALTER TABLE `exercici`
  ADD CONSTRAINT `exercici_ibfk_1` FOREIGN KEY (`Id_Categoria`) REFERENCES `categoria` (`Id`);

--
-- Filtros para la tabla `resultat`
--
ALTER TABLE `resultat`
  ADD CONSTRAINT `resultat_ibfk_1` FOREIGN KEY (`Id_Wo_Usr`) REFERENCES `wo_usr` (`Id`),
  ADD CONSTRAINT `resultat_ibfk_2` FOREIGN KEY (`Id_Exercici`) REFERENCES `exercici` (`Id`);

--
-- Filtros para la tabla `rutineslin`
--
ALTER TABLE `rutineslin`
  ADD CONSTRAINT `rutineslin_ibfk_1` FOREIGN KEY (`Id_Exercici`) REFERENCES `exercici` (`Id`),
  ADD CONSTRAINT `rutineslin_ibfk_2` FOREIGN KEY (`Id_Rutina`) REFERENCES `rutina` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `traduccions`
--
ALTER TABLE `traduccions`
  ADD CONSTRAINT `traduccions_ibfk_1` FOREIGN KEY (`Id_Idioma`) REFERENCES `idioma` (`Id`);

--
-- Filtros para la tabla `usuaris`
--
ALTER TABLE `usuaris`
  ADD CONSTRAINT `usuaris_ibfk_1` FOREIGN KEY (`Id_Idioma`) REFERENCES `idioma` (`Id`);

--
-- Filtros para la tabla `wo_usr`
--
ALTER TABLE `wo_usr`
  ADD CONSTRAINT `wo_usr_ibfk_1` FOREIGN KEY (`Id_Username`) REFERENCES `usuaris` (`Username`),
  ADD CONSTRAINT `wo_usr_ibfk_2` FOREIGN KEY (`Id_Rutina`) REFERENCES `rutina` (`Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
